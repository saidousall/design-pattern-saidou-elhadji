// La  Demo du pattern
class PatternPond {
    public static void main(String[] args)
    {
        Vehicule Vehicule1 = new Voiture(new Produire(), new Assembler());
        Vehicule1.fabriquer();
        Vehicule Vehicule2 = new Moto(new Produire(), new Assembler());
        Vehicule2.fabriquer();
          }
}