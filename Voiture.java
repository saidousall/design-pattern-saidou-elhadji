class Voiture extends Vehicule {
    public Voiture(Atelier Atelier1, Atelier Atelier2)
    {
        super(Atelier1, Atelier2);
    }
  
    @Override
    public void fabriquer()
    {
        System.out.print("Voiture ");
        Atelier1.bosser();
        Atelier2.bosser();
    }
}