// deuxieme implementation concrete du pattern pond
class Assembler implements Atelier {
    @Override
    public void bosser()
    {
        System.out.print(" et");
        System.out.println(" assemblé.");
    }
}
