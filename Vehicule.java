abstract class Vehicule {
    protected Atelier Atelier1;
    protected Atelier Atelier2;
  
    protected Vehicule(Atelier Atelier1, Atelier Atelier2)
    {
        this.Atelier1 = Atelier1;
        this.Atelier2 = Atelier2;
    }
  
    abstract public void fabriquer();
}